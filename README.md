# Rudimentary File Explorer

Wrote a simple file explorer in C# .NET 8 using WinForm.

To build and run: 

```powershell
dotnet run --project .\FileExplorer.csproj
```
